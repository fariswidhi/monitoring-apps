
<?php 
ob_start();


session_start();



if (!empty($_SESSION['username'])) {
  # code...
  // $_SESSION['sukses'] = "Selamat Datang";
  header("location: index.php");
  die;
}
include('conf/config.php');

 ?>

<?php if ($_POST): 
$username  = $_POST['username'];
$password = $_POST['password'];


$sql = "SELECT * FROM user WHERE username = '$username' AND password=md5('".$password."')";

$query = mysqli_query($conn,$sql);
$data = mysqli_fetch_object($query);
$num = mysqli_num_rows($query);
if ($num >0) {
  $_SESSION['username'] = $data->username;

$_SESSION['sukses'] = "Berhasil Login";
header("location: index.php");
}
else{
$_SESSION['gagal'] = "Maaf, Username/Sandi anda Salah";
header("location: login.php");
}
?>


<?php else: ?>

<!DOCTYPE html>
<html>
<head>
  <title>Login</title>


  <link rel="stylesheet" href="<?php echo $base ?>assets/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo $base ?>assets/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo $base ?>assets/bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo $base ?>assets/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo $base ?>assets/dist/css/skins/_all-skins.min.css">
  <!-- Morris chart -->
  <link rel="stylesheet" href="<?php echo $base ?>assets/bower_components/morris.js/morris.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="<?php echo $base ?>assets/bower_components/jvectormap/jquery-jvectormap.css">
  <!-- Date Picker -->
  <link rel="stylesheet" href="<?php echo $base ?>assets/bower_components/bootstrap-datepicker/assets/dist/css/bootstrap-datepicker.min.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="<?php echo $base ?>assets/bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="<?php echo $base ?>assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">

</head>
<body>


<body class="hold-transition login-page">
<div class="login-box">

  <!-- /.login-logo -->
  <div class="login-box-body"  >
<!--     <p class="login-box-msg">Sign in to start your session</p> -->
  <div class="login-logo">
    <a href="<?php echo $base ?>"><b>MONITORING</b></a>
  </div>
   <!-- /.row (main row) -->
      <?php if (isset($_SESSION['sukses'])): ?>
        <div class="alert alert-success">
          <?php echo $_SESSION['sukses'];
          unset($_SESSION['sukses']);
          ?>
        </div>
      <?php endif ?>
      <?php if (isset($_SESSION['gagal'])): ?>
        <div class="alert alert-danger">
          <?php echo $_SESSION['gagal'];
          unset($_SESSION['gagal']);
           ?>
        </div>
      <?php endif ?>
    <form action="" method="post">
      <div class="form-group has-feedback">
        <input type="text" class="form-control" placeholder="Username" name="username">
        <span class="glyphicon glyphicon-user  form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" class="form-control" placeholder="Password" name="password">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="row">
        
        <!-- /.col -->
        <div class="col-xs-12">
        <br>
          <button type="submit" class="btn btn-primary btn-block btn-flat">LOGIN</button>
        </div>
        <!-- /.col -->
      </div>
    </form>

  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

<!-- jQuery 3 -->
<script src="../../bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="../../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="../../plugins/iCheck/icheck.min.js"></script>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' /* optional */
    });
  });
</script>


</body>
</body>
</html>

<?php endif ?>
